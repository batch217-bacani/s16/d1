console.log("Hello World!");

// [SECTION] Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of Modulo operator: " + remainder);

// [SECTION] Assignment Operator.
// Basic Assignment operator is (=) equal sign.

let assignmentNumber = 8;

// Addition Assignment
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// ShortHand for assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator ( -=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses


let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// incrementation and decrementation
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// pre-incrementation
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of post-increment: " + z);

// post-incrementation
increment = z++;
console.log("Result of post-increment: " + increment);

//decrementation
//pre-decrementation
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

//post-decrementation
decrement = z--
console.log("Result of post-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);


// [SECTION] Type Coercion
// Type Coercion is the automatic or implicit conversion of values from one data type to another.

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// true = 1
let numE = true + 1;
console.log(numE);

// false = 0
let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators
let juan = "juan";

// Equality Operator (==)
// = assignment of value
// == Comparison Equality

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
// ! = not

console.log(1 != 1);
console.log(1 != 2);

// Strict Equality Operator (===)
	/*
			-Checks if the value or operand are equal are of the same type
	*/

	console.log(1 === '1')
	console.log('juan' === juan);
	console.log(0 === false);

//Strict Inequality Operator

console.log(1 !== '1');
console.log('juan' !== juan);
console.log(0 !== false);

//[SECTION] Relational Operator
//Some comparison operators check whether one value is greater or less than to the other value.
// > and <

let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// [SECTION] Logical Operators
// && --> AND, || --> OR, ! --> NOT

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical OR Result: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Logical NOT Result: " + someRequirementsNotMet);


